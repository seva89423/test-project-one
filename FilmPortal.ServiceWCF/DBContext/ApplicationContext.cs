﻿using Microsoft.EntityFrameworkCore;

namespace FilmPortal.ServiceWCF.DBContext
{
    public class ApplicationContext : DbContext
    {
        public DbSet<FilmEntity> FilmTable { get; set; }
        public ApplicationContext()
        {
            Database.EnsureCreated();
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseNpgsql("Host=localhost;Port=5432;Database=FilmDB;Username=postgres;Password=test");
        }
    }
}