﻿
namespace FilmPortal.ServiceWCF.DBContext
{
    public class FilmEntity
    {
        public int id { get; set; }

        public int FilmYear { get; set; }

        public string FilmName { get; set; }

        public string FilmDescription { get; set; }

        public string FilmRate { get; set; }

        public string ImageLink { get; set; }

        public override string ToString()
        {
            return $"{id},{FilmYear},{FilmName}";
        }
    }
}
