﻿using FilmPortal.ServiceWCF.DBContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;

namespace FilmPortal.ServiceWCF
{
    [ServiceContract(Namespace = "http://localhost/FilmPortal.ServiceWCF")]
    public interface IFilmWCF
    {
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "FilmAddInfo?filmName={filmName}&filmDescription={filmDescription}&filmYear={filmYear}&filmRate={filmRate}")]
        string FilmAddInfo(string filmName, string filmDescription, int filmYear, string filmRate);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "FilmRemoveInfo?id={id}")]
        string FilmRemoveInfo(int id);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "FilmUpdateInfo?filmName={filmName}&filmDescription={filmDescription}&filmYear={filmYear}&filmRate={filmRate}&id={id}")]
        string FilmUpdateInfo(string filmName, string filmDescription, int filmYear, string filmRate, int id);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "FilmReadAllInfo?all")]
        List<FilmEntity> FilmReadAllInfo();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "FilmFindInfo?id={id}")]
        FilmEntity FilmFindInfo(int id);
    }
}
