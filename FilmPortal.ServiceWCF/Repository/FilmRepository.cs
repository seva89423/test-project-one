﻿using FilmPortal.ServiceWCF.DBContext;
using FilmPortal.ServiceWCF.DTO;
using FilmPortal.ServiceWCF.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FilmPortal.Repository
{
    public class FilmRepository : IFilmRepository
    {
        private ApplicationContext _context = new ApplicationContext();
        public FilmRepository()
        {

        }

        public void FilmAddInfo(FilmEntity film)
        {
            var entities = _context.FilmTable.ToList();
            film.id = entities.Max(item => item.id) + 1;
            _context.FilmTable.Add(film);
            _context.SaveChanges();
        }

        public FilmEntity FilmFindInfo(int Id)
        {
            var id = Id;
            var entity =  _context.FilmTable.Find(id);
            return entity;
        }

        public List<FilmEntity> FilmReadAllInfo()
        {
            return _context.FilmTable.ToList();
        }

        public void FilmRemoveInfo(int id)
        {
            var entity = _context.FilmTable.Find(id);
            _context.FilmTable.Remove(entity);
            _context.SaveChanges();
        }

        public void FilmUpdateInfo(FilmDTO film, int id)
        {
            var entity = _context.FilmTable.Find(id);
            entity.FilmName = film.FilmName;
            entity.FilmDescription = film.FilmDescription;
            entity.FilmRate = film.FilmRate;
            entity.FilmYear = film.FilmYear;
            entity.ImageLink = film.ImageLink;
            _context.FilmTable.Update(entity);
            _context.SaveChanges();
        }
    }
}
