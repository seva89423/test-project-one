﻿using FilmPortal.ServiceWCF.DBContext;
using FilmPortal.ServiceWCF.DTO;
using System.Collections.Generic;

namespace FilmPortal.ServiceWCF.Interfaces
{
    public interface IFilmRepository
    {
        void FilmAddInfo(FilmEntity film);

        void FilmRemoveInfo(int id);

        void FilmUpdateInfo(FilmDTO film, int id);

        List<FilmEntity> FilmReadAllInfo();

        FilmEntity FilmFindInfo(int userId);
    }
}
