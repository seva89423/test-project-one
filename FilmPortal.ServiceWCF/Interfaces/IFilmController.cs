﻿using FilmPortal.ServiceWCF;
using FilmPortal.ServiceWCF.DBContext;
using System.Collections.Generic;

namespace FilmPortal.ServiceWCF.Interfaces
{
    public interface IFilmController
    {
        void FilmAddInfo(string filmName, string filmDescription, int filmYear, string filmRate, string imageLink);

        void FilmRemoveInfo(int id);

        void FilmUpdateInfo(string filmName, string filmDescription, int filmYear, string filmRate, int id, string imageLink);

        List<FilmEntity> FilmReadAllInfo();

        FilmEntity FilmFindInfo(int userId);
    }
}
