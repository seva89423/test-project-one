﻿using FilmPortal.ServiceWCF.DBContext;
using FilmPortal.ServiceWCF.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FilmPortal.ServiceWCF.Interfaces
{
    public interface IFilmService
    {
        void FilmAddInfo(FilmDTO film);

        void FilmRemoveInfo(int id);

        void FilmUpdateInfo(FilmDTO film, int id);

        List<FilmEntity> FilmReadAllInfo();

        FilmEntity FilmFindInfo(int userId);
    }
}
