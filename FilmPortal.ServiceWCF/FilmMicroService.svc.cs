﻿using FilmPortal.Controllers;
using FilmPortal.ServiceWCF.DBContext;
using FilmPortal.ServiceWCF.DTO;
using FilmPortal.ServiceWCF.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace FilmPortal.ServiceWCF
{
    [ServiceBehavior(IncludeExceptionDetailInFaults = true)]
    public class FilmMicroService : IFilmMicroService
    {
        #region Main
        private FilmController _controller { get; set; } = new FilmController();

        private PremierService _service { get; set; } = new PremierService();

        public string FilmAddInfo(string filmName, string filmDescription, int filmYear, string filmRate, string imageLink)
        {
            try
            {
                _controller.FilmAddInfo(filmName, filmDescription, filmYear, filmRate, imageLink);
                return "Фильм добавлен";
            } catch 
            {
                return "Что-то пошло не так. Повторите попытку позже.";
            }
        }

        public FilmEntity FilmFindInfo(int id)
        {
            return _controller.FilmFindInfo(id);
        }

        public List<FilmEntity> FilmReadAllInfo()
        {
            return _controller.FilmReadAllInfo();
        }

        public string FilmRemoveInfo(int id)
        {
            try
            {
                _controller.FilmRemoveInfo(id);
                return "Фильм удалён";
            } catch 
            {
                return "Что-то пошло не так. Повторите попытку позже.";
            }
        }

        public string FilmUpdateInfo(string filmName, string filmDescription, int filmYear, string filmRate, int id, string imageLink)
        {
                if (filmName != null || filmDescription != null || filmRate != null || filmYear != 0 || id != 0) { 
                 _controller.FilmUpdateInfo(filmName, filmDescription, filmYear, filmRate, id, imageLink);
                return "Фильм обновлён";
            } else 
            {
                return "Что-то пошло не так. Повторите попытку позже.";
            }
            
        }

        public List<PremierDTO> PremiersList()
        {
            return _service.FindPremiers().Result;
        }

        public List<PremierDTO> PremiersInRussiaList() 
        {
            return _service.FindPremiersRus().Result;
        }
        #endregion
    }
}
