﻿using FilmPortal.ServiceWCF.DBContext;
using FilmPortal.ServiceWCF.DTO;
using FilmPortal.ServiceWCF.Interfaces;
using FilmPortal.Service;
using System.Collections.Generic;

namespace FilmPortal.Controllers
{
    public class FilmController : IFilmController
    {
        private FilmService _service = new FilmService();
        public void FilmAddInfo(string filmName, string filmDescription, int filmYear, string filmRate, string imageLink)
        {
            var film = new FilmDTO() {
            FilmName = filmName,
            FilmDescription = filmDescription,
            FilmYear = filmYear,
            FilmRate = filmRate,
            ImageLink = imageLink ?? "No image"
            };
            _service.FilmAddInfo(film);
        }

        public FilmEntity FilmFindInfo(int id)
        {
            return _service.FilmFindInfo(id);
        }

        public List<FilmEntity> FilmReadAllInfo()
        {
            return _service.FilmReadAllInfo();
        }

        public void FilmRemoveInfo(int id)
        {
            _service.FilmRemoveInfo(id);
        }

        public void FilmUpdateInfo(string filmName, string filmDescription, int filmYear, string filmRate, int id, string imageLink)
        {
            var film = new FilmDTO()
            {
                FilmName = filmName,
                FilmDescription = filmDescription,
                FilmYear = filmYear,
                FilmRate = filmRate,
                ImageLink = imageLink ?? "No image"
            };
            _service.FilmUpdateInfo(film, id);
        }
    }
}