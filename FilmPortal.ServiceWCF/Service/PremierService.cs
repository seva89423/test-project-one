﻿using AngleSharp;
using FilmPortal.ServiceWCF.DBContext;
using FilmPortal.ServiceWCF.DTO;
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;

namespace FilmPortal.ServiceWCF.Service
{
    public class PremierService
    {
        private WebClient _client { get; set; }
        private HtmlDocument _documentHtml { get; set; }
        private HtmlNodeCollection _collection { get; set; }
        private List<PremierDTO> _films { get; set; } = new List<PremierDTO>();

        private List<PremierDTO> _filmsInRussia { get; set; } = new List<PremierDTO>();
        private string _query { get; set; }
        public PremierService()
        {
            _client = new WebClient();
            _documentHtml = new HtmlDocument();
        }

        public async Task<List<PremierDTO>> FindPremiersRus() 
        {
            string url = "https://www.kinopoisk.ru/afisha/new/";
            var browserConfig = Configuration.Default.WithDefaultLoader();
            using (var context = BrowsingContext.New(browserConfig))
            using (var doc = await context.OpenAsync(url))
            {
                var parsedFilms = doc.GetElementsByClassName("filmsListNew")[0].GetElementsByClassName("item");
                for (int i = 0; i < parsedFilms.Length; i++)
                {
                    var name = parsedFilms[i].GetElementsByClassName("info")[0].GetElementsByClassName("name")[0].TextContent.Trim().Split('\n');
                    var director = parsedFilms[i].GetElementsByClassName("info")[0].GetElementsByClassName("gray")[0].QuerySelector("a").TextContent.Trim();
                    var imageLink = parsedFilms[i].GetElementsByClassName("poster")[0].QuerySelector("a").GetElementsByTagName("img")[0].GetAttribute("src");
                    _filmsInRussia.Add(new PremierDTO() 
                    {
                        Id = i,
                        Name = name[0],
                        Description = "Releases in Russia",
                        Director = director,
                        ImageLink = imageLink,
                        Year = "2022",

                    });
                }
            }
            if (_filmsInRussia == null) return null;
            return _filmsInRussia;
          }
        public async Task<List<PremierDTO>> FindPremiers()
        {
            string url = "https://www.imdb.com/movies-coming-soon/";
            var browserConfig = Configuration.Default.WithDefaultLoader();
            using (var context = BrowsingContext.New(browserConfig))
            using (var doc = await context.OpenAsync(url))
            {
                var parsedFilms = doc.GetElementsByClassName("list detail")[0].GetElementsByClassName("list_item");
                for (int i = 0; i < parsedFilms.Length; i++)
                {
                    string premierName = parsedFilms[i].QuerySelector("h4").TextContent.Trim();

                    var premierDescription = parsedFilms[i].GetElementsByClassName("outline")[0].TextContent.Trim();
                    var imageLink = parsedFilms[i].GetElementsByClassName("poster")[0].GetAttribute("src").Trim();

                    _films.Add(new PremierDTO()
                    {
                        Id = i,
                        Name = premierName.Remove(premierName.Length - 7, 7),
                        Director = parsedFilms[i].GetElementsByClassName("txt-block")[0].QuerySelector("a").TextContent.Trim(),
                        Description = premierDescription,
                        Year = premierName.Substring(premierName.Length - 5, 4),
                        ImageLink = imageLink
                    });
                }
            }
            if (_films == null) return null;
            return _films;
        }
    }
}