﻿using FilmPortal.ServiceWCF.DBContext;
using FilmPortal.ServiceWCF.DTO;
using FilmPortal.ServiceWCF.Interfaces;
using FilmPortal.Repository;
using System;
using System.Collections.Generic;

namespace FilmPortal.Service
{
    public class FilmService : IFilmService
    {
        private FilmRepository _repository = new FilmRepository();
        public FilmService()
        {

        }

        public void FilmAddInfo(FilmDTO film)
        {
            var newEntity = new FilmEntity()
            {
                FilmName = film.FilmName,
                FilmDescription = film.FilmDescription,
                FilmYear = film.FilmYear,
                FilmRate = film.FilmRate,
                ImageLink = film.ImageLink
            };
            _repository.FilmAddInfo(newEntity);


        }

        public FilmEntity FilmFindInfo(int id)
        {
            return _repository.FilmFindInfo(id);
        }

        public List<FilmEntity> FilmReadAllInfo()
        {
            return _repository.FilmReadAllInfo();
        }

        public void FilmRemoveInfo(int id)
        {
            _repository.FilmRemoveInfo(id);
        }

        public void FilmUpdateInfo(FilmDTO film, int id)
        {
            _repository.FilmUpdateInfo(film, id);
        }
    }
}