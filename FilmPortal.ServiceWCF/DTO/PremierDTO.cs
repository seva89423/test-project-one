﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FilmPortal.ServiceWCF.DTO
{
    public class PremierDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Director { get; set; }
        public string Description { get; set; }
        public string Year { get; set; }
        public string ImageLink { get; set; }
    }
}