﻿
namespace FilmPortal.ServiceWCF.DTO
{
    public class FilmDTO
    {
        public string FilmName { get; set; }

        public string FilmDescription { get; set; }

        public int FilmYear { get; set; }

        public string FilmRate { get; set; }

        public string ImageLink { get; set; }
    }
}